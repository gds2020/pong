﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PongGame : MonoBehaviour
{
    public GameObject tableObject;
    public GameObject ballObject;
    public float ballSpeed = 1f;
    public float playerOffset = 1f;
    public GameObject playerRight;
    public GameObject playerLeft;
    public float playerLeftSpeed = 1f;
    public float playerRightSpeed = 1f;
    public float deviationBorderAngle;
    public float playerMaxBounceAngle;
    public float ballSpeedIncreaseAmmount;
    public float ballSpeedBounceDelta;
    public TextMesh textMesh;
    public TextMesh gameStatsTextMesh;

    Vector2 playAreaSize;
    Vector2 tableAreaSize;
    float ballRadius;
    Vector3 ballDirection;
    bool isDefeat = false;
    int rightWinCount;
    int leftWinCount;
    int roundsCount;
    float currentBallSpeed;


    void Start()
    {
        ballRadius = 0.5f * ballObject.transform.localScale.x;

        Vector3 tableScale = tableObject.transform.localScale;
        playAreaSize = new Vector2(tableScale.x, tableScale.y);
        tableAreaSize = playAreaSize;

        Restart();

        playAreaSize -= new Vector2(ballRadius, ballRadius) * 2f;

        Debug.Log("table area size is: " + playAreaSize);
        Debug.Log("ball radius is: " + ballRadius);
    }


    void Update()
    {
        MoveBall();
        if (isDefeat == false)
        {
            CheckBounds();
            ProcessInputPlayerLeft();
            ProcessInputPlayerRight();
        }
    }


    void MoveBall()
    {
        if (currentBallSpeed < 5f)
            currentBallSpeed = 5f;
        ballObject.transform.position += ballDirection * Time.deltaTime * currentBallSpeed;
        currentBallSpeed += ballSpeedIncreaseAmmount * Time.deltaTime;
    }

    void CheckBounds()
    {
        float x = ballObject.transform.position.x;
        float halfSizeX = playAreaSize.x / 2f;

        float rightBorder = tableAreaSize.x / 2f - playerOffset - playerRight.transform.localScale.x * 0.5f;
        rightBorder -= ballRadius;
        float leftBorder = -tableAreaSize.x / 2f + playerOffset + playerLeft.transform.localScale.x * 0.5f;
        leftBorder += ballRadius;


        if (x > rightBorder)
        {
            float upBorder = playerRight.transform.position.z + playerRight.transform.localScale.z / 2f;
            float downBorder = playerRight.transform.position.z - playerRight.transform.localScale.z / 2f;

            if (ballObject.transform.position.z <= upBorder && ballObject.transform.position.z >= downBorder)
            {
                ballDirection.x = -ballDirection.x;
                float posZDifference = ballObject.transform.position.z - playerRight.transform.position.z;
                float bounceFactor = posZDifference / playerRight.transform.localScale.z / 2f; // [-1,1]
                ballDirection = Quaternion.Euler(0f, bounceFactor * playerMaxBounceAngle, 0f) * ballDirection;
                Vector3 pos = ballObject.transform.position;
                pos.x = rightBorder;
                ballObject.transform.position = pos;

                if (Input.GetKey(KeyCode.LeftArrow))
                    currentBallSpeed += ballSpeedBounceDelta;
                if (Input.GetKey(KeyCode.RightArrow))
                    currentBallSpeed -= ballSpeedBounceDelta;
            }
            else
            {
                SetDefeat(false);
            }
        }
        else if (x < leftBorder)
        {
            float upBorder = playerLeft.transform.position.z + playerLeft.transform.localScale.z / 2f;
            float downBorder = playerLeft.transform.position.z - playerLeft.transform.localScale.z / 2f;

            if (ballObject.transform.position.z <= upBorder && ballObject.transform.position.z >= downBorder)
            {
                ballDirection.x = -ballDirection.x;
                float posZDifference = ballObject.transform.position.z - playerLeft.transform.position.z;
                float bounceFactor = -posZDifference / playerLeft.transform.localScale.z / 2f; // [-1,1]
                ballDirection = Quaternion.Euler(0f, bounceFactor * playerMaxBounceAngle, 0f) * ballDirection;
                Vector3 pos = ballObject.transform.position;
                pos.x = leftBorder;
                ballObject.transform.position = pos;

                if (Input.GetKey(KeyCode.D))
                    currentBallSpeed += ballSpeedBounceDelta;
                if (Input.GetKey(KeyCode.A))
                    currentBallSpeed -= ballSpeedBounceDelta;
            }
            else
            {
                SetDefeat(true);
            }
        }

        float y = ballObject.transform.position.z;
        float halfSizeY = playAreaSize.y / 2f;

        if (y > halfSizeY || y < -halfSizeY)
        {
            ballDirection.z = -ballDirection.z;
            float angle = Random.Range(-deviationBorderAngle, deviationBorderAngle);
            ballDirection = Quaternion.Euler(0f, angle, 0f) * ballDirection;
            // set object on the border edge
            Vector3 pos = ballObject.transform.position;
            pos.z = halfSizeY * Mathf.Sign(pos.z);
            ballObject.transform.position = pos;
        }
    }


    void ProcessInputPlayerLeft()
    {
        float speedMult = 1f;
        if (Input.GetKey(KeyCode.LeftShift))
            speedMult = 2f;
        if (Input.GetKey(KeyCode.W))
            playerLeft.transform.Translate(0f, 0f, playerLeftSpeed * speedMult * Time.deltaTime);
        if (Input.GetKey(KeyCode.S))
            playerLeft.transform.Translate(0f, 0f, -playerLeftSpeed * speedMult * Time.deltaTime);

        float playerHalfHeight = playerLeft.transform.localScale.z * 0.5f;
        float tableHalfHeight = tableAreaSize.y / 2f;

        if (playerLeft.transform.position.z > 0f)
        {
            float diff = tableHalfHeight - playerLeft.transform.position.z - playerHalfHeight;
            if (diff < 0f )
            {
                Vector3 pos = playerLeft.transform.position;
                pos.z = tableHalfHeight - playerHalfHeight;
                playerLeft.transform.position = pos;
            }
        }
        else
        {
            float diff = -tableHalfHeight - playerLeft.transform.position.z + playerHalfHeight;
            if (diff > 0f)
            {
                Vector3 pos = playerLeft.transform.position;
                pos.z = -tableHalfHeight + playerHalfHeight;
                playerLeft.transform.position = pos;
            }
        }
    }

    void ProcessInputPlayerRight()
    {
        float speedMult = 1f;
        if (Input.GetKey(KeyCode.RightShift))
            speedMult = 2f;

        if (Input.GetKey(KeyCode.UpArrow))
            playerRight.transform.Translate(0f, 0f, playerRightSpeed * speedMult * Time.deltaTime);
        if (Input.GetKey(KeyCode.DownArrow))
            playerRight.transform.Translate(0f, 0f, -playerRightSpeed * speedMult * Time.deltaTime);

        float playerHalfHeight = playerRight.transform.localScale.z * 0.5f;
        float tableHalfHeight = tableAreaSize.y / 2f;

        if (playerRight.transform.position.z > 0f)
        {
            float diff = tableHalfHeight - playerRight.transform.position.z - playerHalfHeight;
            if (diff < 0f )
            {
                Vector3 pos = playerRight.transform.position;
                pos.z = tableHalfHeight - playerHalfHeight;
                playerRight.transform.position = pos;
            }
        }
        else
        {
            float diff = -tableHalfHeight - playerRight.transform.position.z + playerHalfHeight;
            if (diff > 0f)
            {
                Vector3 pos = playerRight.transform.position;
                pos.z = -tableHalfHeight + playerHalfHeight;
                playerRight.transform.position = pos;
            }
        }
    }


    void Restart()
    {
        roundsCount++;
        isDefeat = false;
        currentBallSpeed = ballSpeed;
        ballDirection = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-0.2f, 0.2f));
        ballDirection.Normalize();
        //ballDirection = new Vector3(0.01f,0,1);
        // place ball on table
        Vector3 ballPos = Vector3.zero;
        ballPos.y = tableObject.transform.position.y + ballRadius;
        ballObject.transform.position = ballPos;
        // place left player on table
        Vector3 playerLeftPos = new Vector3(-playAreaSize.x / 2f + playerOffset, 0f, 0f);
        playerLeftPos.y = tableObject.transform.position.y + playerLeft.transform.localScale.y / 2f;
        playerLeft.transform.position = playerLeftPos;
        // place right player on table
        Vector3 playerRightPos = new Vector3(playAreaSize.x / 2f - playerOffset, 0f, 0f);
        playerRightPos.y = tableObject.transform.position.y + playerRight.transform.localScale.y / 2f;
        playerRight.transform.position = playerRightPos;
        // update ui text
        string uiText = string.Format("round {0}   {1}:{2}", roundsCount, leftWinCount, rightWinCount);
        textMesh.text = uiText;
        gameStatsTextMesh.text = "";
    }


    void SetDefeat(bool rightPlayerWins)
    {
        isDefeat = true;
        if (rightPlayerWins)
        {
            rightWinCount++;
        }
        else
        {
            leftWinCount++;
        }

        gameStatsTextMesh.text = string.Format("Player {0} Wins",
            rightPlayerWins? "Right":"Left");
        Invoke(nameof(Restart), 2f);
    }
}
